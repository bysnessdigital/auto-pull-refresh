const { spawn } = require('child_process');
let os = require('os');
module.exports=()=>{
    return new function () {
        this.spawnScript=(scriptName, extraArgs=[])=>{
            let browserProcess;
            try {
                if(os.platform()==='win32')
                {
                    browserProcess=spawn('sh',[scriptName,...extraArgs],{env:process.env});
                }
                else if(os.platform()==='linux')
                {
                    process.env['DISPLAY']=':0';
                    browserProcess=spawn('sh',[scriptName,...extraArgs],{env:process.env});
                }
                else{
                    console.error(os.platform() + ' is not yet supported as script host');
                }
            }
            catch(e){
                console.error(`Error during spawning for ${scriptName} with extraArgs ${extraArgs}`,e);
            }
            return browserProcess;
        };
    };
}