/**
 * Created by Anuraag on 8th Sep 2018.
 */
let miniServer={
    create:function(common,port)
    {
        return new function(){
            let This=this;

            This.express = require('express');
            let path = require('path');
            let favicon = require('serve-favicon');
            let cookieParser = require('cookie-parser');
            let bodyParser = require('body-parser');
            let http = require('http');
            let uuid = require('node-uuid');
            /**
             * Module dependencies.
             */

            This.app = This.express();
            //console.log(This.app);

            //todo: this CORS needs to change
            This.app.use(function(req, res, next) {
                res.header("Access-Control-Allow-Origin", "*");
                next();
            });
            // view engine setup
            // This.app.set('views', path.join(__dirname, 'views'));
            This.app.set('view engine', 'ejs');

            // uncomment after placing your favicon in /static
            //This.app.use(favicon(path.join(__dirname, 'static', 'favicon.ico')));
            //This.app.use(logger('dev'));
            This.app.use(bodyParser.json());
            This.app.use(bodyParser.urlencoded({ extended: false }));
            let cookieParserObj=cookieParser();
            This.app.use(cookieParserObj);

            This.attachErrorHandlers=()=>{
                // catch 404 and forward to error handler
                This.app.use(function(req, res, next) {
                    let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
                    throw new Error('404 - Not found - ' + fullUrl);
                });

                // error handlers

                // development error handler
                // will print stacktrace
                if (This.app.get('env') === 'development') {
                    This.app.use(function(err, req, res, next) {
                        console.log('error is',err.message);
                        res.status(err.status || 404);
                        res.write('The page handler is not registered.');
                        res.end();
                        // res.render('land', { //todo: change back to land.ejs
                        //     message: err.message,
                        //     error: err.stack
                        // });
                        console.error(err.stack || err);
                    });
                }
            };
            This.server = http.createServer(This.app);

            /**
             * Listen on provided port, on all network interfaces.
             */
            This.server.listen(port);

            This.server.on('error', onError);
            This.server.on('listening', onListening);

            /**
             * Normalize a port into a number, string, or false.
             */
            function normalizePort(val) {
                let port = parseInt(val, 10);

                if (isNaN(port)) {
                    // named pipe
                    return val;
                }

                if (port >= 0) {
                    // port number
                    return port;
                }

                return false;
            }

            /**
             * Event listener for HTTP server "error" event.
             */
            function onError(error) {
                //throw error;
                if (error.syscall !== 'listen') {
                    throw error;
                }

                let bind = typeof port === 'string'
                    ? 'Pipe ' + port
                    : 'Port ' + port;

                // handle specific listen errors with friendly messages
                switch (error.code) {
                    case 'EACCES':
                        console.error(bind + ' requires elevated privileges');
                        process.exit(1);
                        break;
                    case 'EADDRINUSE':
                        console.error(bind + ' is already in use');
                        //process.exit(1);
                        break;
                    default:
                        throw error;
                }
            }

            /**
             * Event listener for HTTP server "listening" event.
             */
            function onListening() {
                let addr = This.server.address();
                let bind = typeof addr === 'string'
                    ? 'pipe ' + addr
                    : 'port ' + addr.port;
                console.log('Listening on ' + bind);
            }
        };
    }
};
module.exports=miniServer;