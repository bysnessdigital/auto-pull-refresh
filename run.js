let common={};
let argv = require('minimist')(process.argv.slice(2));
common.server=require('./server').create(common,argv.port || 2022);
let refresh=require('./refresh')();
let router=common.server.express.Router();
common.mainRouter=router;
common.server.app.use('/',router);
let handler=(req,res)=>{
    console.log('pulling',new Date());
    let output=refresh.spawnScript(argv.scriptName || 'refresh.sh',argv.extraArgs?argv.extraArgs.split(';'):[]);
    output.stdout.pipe(res);
    res.write("Current Timestamp: " + ( + new Date()) + " \n");
};
common.mainRouter.post('/refresh',handler);
common.mainRouter.get('/refresh',handler);
common.server.attachErrorHandlers();